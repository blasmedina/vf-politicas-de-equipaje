<?php

define('DS', DIRECTORY_SEPARATOR);

if (!function_exists('debug')) {
	function debug($value) {
		print('<pre>');
		print_r($value);
		print('</pre>');
	}
}

if (!function_exists('multi_implode')) {
	function multi_implode($glue, $array) {
		$ret = "";
		foreach ($array as $item) {
			if (is_array($item)) {
				$ret .= multi_implode($glue, $item) . $glue;
			} else {
				$ret .= $item . $glue;
			}
		}
		$ret = substr($ret, 0, 0 - strlen($glue));
		return $ret;
	}
}

/**
* 
*/
class GeneraQueryPoliticas
{
	static $nombre;
	static $fileCSV;
	static $fileSQL;

	static $data;
	static $reglasAgrupadas;

	static $posNombreTarifa = 0;
	static $posDescripcionTarifa = 1;
	static $posPrestador = 2;
	static $posFarebasis = 3;
	static $posContinenteOrigen = 4;
	static $posPaisOrigen = 5;
	static $posCiudadOrigen = 6;
	static $posContinenteDestino = 7;
	static $posPaisDestino = 8;
	static $posCiudadDestino = 9;
	static $posPortalCL = 10;
	static $posPortalAR = 11;
	static $posPortalPE = 12;
	static $posPortalCO = 13;
	static $posTipoMensaje = 14;
	static $posTitulo = 15;
	static $posMensaje = 16;
	static $posValor = 17;
	static $null = "NULL";
	static $tipoMensajes = ["equipaje_bodega", "equipaje_cabina", "reembolsos", "preseleccion_asiento", "cambio_itinerario", "upgrade"];
	static $query = [];

	static function procesarDataLinea($dataLinea){
		$data = array_map("trim", $dataLinea);

		if($data[self::$posFarebasis] !== "") {
			$data[self::$posFarebasis] = array_map("trim", explode(",", $data[self::$posFarebasis]));
		} else {
			$data[self::$posFarebasis] = [];
		}
		return $data;
	}

	static function convierteCsvEnArray() {
		$lineas = file(self::$fileCSV);
		self::$data = [];
		$deliminador = ",";
		foreach ($lineas as $numeroLinea => $linea) {
			$dataLinea = str_getcsv($linea, $deliminador);
			if($numeroLinea > 0) {
				array_push(self::$data, self::procesarDataLinea($dataLinea));
			}
		}
	}

	static function generaHash($value) {
		$baseHash = [];
		array_push($baseHash, $value[self::$posPrestador]);
		array_push($baseHash, $value[self::$posFarebasis]);
		array_push($baseHash, $value[self::$posContinenteOrigen]);
		array_push($baseHash, $value[self::$posPaisOrigen]);
		array_push($baseHash, $value[self::$posCiudadOrigen]);
		array_push($baseHash, $value[self::$posContinenteDestino]);
		array_push($baseHash, $value[self::$posPaisDestino]);
		array_push($baseHash, $value[self::$posCiudadDestino]);
		$hash = multi_implode("-", $baseHash);
		return $hash;
	}

	static function agruparReglas() {
		$ultimoCodigo = null;
		$ultimoHash = null;
		self::$reglasAgrupadas = [];
		foreach (self::$data as $value) {
			$codigo = strtoupper($value[self::$posNombreTarifa]);
			$hash = self::generaHash($value);
			if($ultimoCodigo !== $codigo) {
				$ultimoCodigo = $codigo;
			}
			if($ultimoHash !== $hash) {
				$ultimoHash = $hash;
			}
			if(isset(self::$reglasAgrupadas[$codigo][$hash])) {
				array_push(self::$reglasAgrupadas[$codigo][$hash], $value);
			} else {
				self::$reglasAgrupadas[$codigo][$hash] = [$value];
			}
		}
		self::$reglasAgrupadas;
	}

	static function generarQueryTarifa($regla, $portal) {
		$prestador = $regla[self::$posPrestador];
		$codigo = preg_replace("/(\s)+/i", "_", strtoupper($regla[self::$posNombreTarifa]));
		$campos = [
			"`id_portal`",
			"`codigo`",
			"`nombre`",
			"`descripcion`",
			"`id_continente_origen`",
			"`pais_origen`",
			"`ciudad_origen`",
			"`id_continente_destino`",
			"`pais_destino`",
			"`ciudad_destino`",
			"`id_prestador`"
		];
		$value = [
			sprintf("@id_portal_%s", $portal),
			$codigoTarifa = sprintf("'%s'", $codigo),
			sprintf("'%s'", $regla[self::$posNombreTarifa]),
			sprintf("'%s'", $regla[self::$posDescripcionTarifa]),
			$regla[self::$posContinenteOrigen] === "" ? self::$null : sprintf("@id_continente_%s", $regla[self::$posContinenteOrigen]),
			$regla[self::$posPaisOrigen] === "" ? self::$null : sprintf("'%s'", $regla[self::$posPaisOrigen]),
			$regla[self::$posCiudadOrigen] === "" ? self::$null : sprintf("'%s'", $regla[self::$posCiudadOrigen]),
			$regla[self::$posContinenteDestino] === "" ? self::$null : sprintf("@id_continente_%s", $regla[self::$posContinenteDestino]),
			$regla[self::$posPaisDestino] === "" ? self::$null : sprintf("'%s'", $regla[self::$posPaisDestino]),
			$regla[self::$posCiudadDestino] === "" ? self::$null : sprintf("'%s'", $regla[self::$posCiudadDestino]),
			sprintf("@id_prestador_%s", $prestador)
		];
		$query = sprintf("INSERT INTO `vuelos`.`tarifa` (%s) VALUES (%s);", implode(", ", $campos), implode(", ", $value));
		return $query;
	}

	static function generarQueryFarebasis($regla) {
		$query = "";
		if(sizeof($regla[self::$posFarebasis]) > 0) {
			$campos = [
				"`regla`",
				"`id_tarifa`"
			];
			$query = sprintf("INSERT INTO `vuelos`.`tarifa_regla_farebasis` (%s) VALUES ", implode(", ", $campos));
			$values = [];
			foreach ($regla[self::$posFarebasis] as $farebasis) {
				$value = [
					sprintf("'%s'", $farebasis),
					"@id_tarifa"
				];
				array_push($values, sprintf("(%s)", implode(", ", $value)));
			}
			$query .= implode(", ", $values) . ";";
		}
		return $query;
	}

	static function generarQueryMensaje($reglas) {
		$query = "";
		if(sizeof($reglas) > 0) {
			$campos = [
				"`titulo`",
				"`mensaje`",
				"`valor`",
				"`id_tarifa_mensaje_tipo`",
				"`id_tarifa`"
			];
			$query = sprintf("INSERT INTO `vuelos`.`tarifa_mensaje` (%s) VALUES ", implode(", ", $campos));
			$values = [];
			foreach ($reglas as $regla) {
				self::validaTipoMensaje($regla[self::$posTipoMensaje]);
				$value = [
					sprintf("'%s'", $regla[self::$posTitulo]),
					sprintf("'%s'", $regla[self::$posMensaje]),
					$regla[self::$posValor] === "" ? self::$null : sprintf("%s", $regla[self::$posValor]),
					sprintf("@id_tipo_mensaje_%s", $regla[self::$posTipoMensaje]),
					"@id_tarifa"
				];
				array_push($values, sprintf("(%s)", implode(", ", $value)));
			}
			$query .= implode(", ", $values).";";
		}
		return $query;
	}

	static function identificaPortales($regla) {
		$portales = [];
		if(trim(strtoupper($regla[self::$posPortalCL])) === 'SI') {
			array_push($portales, 'cl');
		}
		if(trim(strtoupper($regla[self::$posPortalAR])) === 'SI') {
			array_push($portales, 'ar');
		}
		if(trim(strtoupper($regla[self::$posPortalCO])) === 'SI') {
			array_push($portales, 'co');
		}
		if(trim(strtoupper($regla[self::$posPortalPE])) === 'SI') {
			array_push($portales, 'pe');
		}
		return $portales;
	}

	static function validaTipoMensaje($tipo) {
		if(!(in_array($tipo, self::$tipoMensajes))) {
			throw new Exception("Error Processing Request {$tipo}", 1);
		}
	}

	static function imprimirReglas() {
		foreach (self::$query as $value) {
			echo sprintf("%s<br>", $value);
		}
	}

	static function identificaPrestadores() {
		$prestadores = array_unique(array_column(self::$data, self::$posPrestador));
		array_push(self::$query, "-- PRESTADORES");
		foreach ($prestadores as $prestador) {
			array_push(self::$query, sprintf("SET @id_prestador_%s = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = '%s');", $prestador, $prestador));
		}
		self::saltoLinea();
	}

	static function saltoLinea() {
		array_push(self::$query, "");
		array_push(self::$query, "");
	}

	static function generarConst() {
		array_push(self::$query, "-- PORTAL");
		array_push(self::$query, "SET @id_portal_cl = (SELECT `id_portal` FROM `viajes_falabella`.`portal` WHERE `viajes_falabella`.`portal`.`codigo_moneda` = 'CLP');");
		array_push(self::$query, "SET @id_portal_ar = (SELECT `id_portal` FROM `viajes_falabella`.`portal` WHERE `viajes_falabella`.`portal`.`codigo_moneda` = 'ARS');");
		array_push(self::$query, "SET @id_portal_co = (SELECT `id_portal` FROM `viajes_falabella`.`portal` WHERE `viajes_falabella`.`portal`.`codigo_moneda` = 'COP');");
		array_push(self::$query, "SET @id_portal_pe = (SELECT `id_portal` FROM `viajes_falabella`.`portal` WHERE `viajes_falabella`.`portal`.`codigo_moneda` = 'PEN');");
		self::saltoLinea();
		array_push(self::$query, "-- CONTINENT");
		array_push(self::$query, "SET @id_continente_NA = (SELECT `idContinent` FROM `viajes_falabella`.`Continent` WHERE `viajes_falabella`.`Continent`.`description` = 'Norteamérica');");
		array_push(self::$query, "SET @id_continente_SA = (SELECT `idContinent` FROM `viajes_falabella`.`Continent` WHERE `viajes_falabella`.`Continent`.`description` = 'Sudamérica');");
		array_push(self::$query, "SET @id_continente_CA = (SELECT `idContinent` FROM `viajes_falabella`.`Continent` WHERE `viajes_falabella`.`Continent`.`description` = 'América Central');");
		array_push(self::$query, "SET @id_continente_EU = (SELECT `idContinent` FROM `viajes_falabella`.`Continent` WHERE `viajes_falabella`.`Continent`.`description` = 'Europa');");
		array_push(self::$query, "SET @id_continente_AF = (SELECT `idContinent` FROM `viajes_falabella`.`Continent` WHERE `viajes_falabella`.`Continent`.`description` = 'África');");
		array_push(self::$query, "SET @id_continente_AS = (SELECT `idContinent` FROM `viajes_falabella`.`Continent` WHERE `viajes_falabella`.`Continent`.`description` = 'Asia');");
		array_push(self::$query, "SET @id_continente_OC = (SELECT `idContinent` FROM `viajes_falabella`.`Continent` WHERE `viajes_falabella`.`Continent`.`description` = 'Oceanía');");
		self::saltoLinea();
	}

	static function generaQuery($reglas, $regla, $portal) {
		array_push(self::$query, sprintf("-- %s - %s - %s", strtoupper($regla[self::$posDescripcionTarifa]), strtoupper($portal), strtoupper($regla[self::$posNombreTarifa])));
		array_push(self::$query, self::generarQueryTarifa($regla, $portal));
		array_push(self::$query, "SET @id_tarifa = (SELECT LAST_INSERT_ID());");
		$queryFarebasis = self::generarQueryFarebasis($regla);
		if($queryFarebasis !== "") {
			array_push(self::$query, "-- REGLAS FAREBASIS");
			array_push(self::$query, self::generarQueryFarebasis($regla));
		}
		array_push(self::$query, "-- MENSAJES");
		array_push(self::$query, self::generarQueryMensaje($reglas));
	}

	static function procesaReglasAgrupadas() {
		foreach (self::$reglasAgrupadas as $reglasHash) {
			foreach ($reglasHash as $reglas) {
				$regla = $reglas[0];
				$portales = self::identificaPortales($regla);
				foreach ($portales as $portal) {
					self::saltoLinea();
					self::generaQuery($reglas, $regla, $portal);
				}
			}
		}
		self::saltoLinea();
	}

	static function fixQuery() {
		array_push(self::$query, "-- FIX CODIGO");
		$concantenar = [];
		array_push($concantenar, "`vuelos`.`tarifa`.`codigo`");
		array_push($concantenar, "COALESCE(`vuelos`.`tarifa`.`id_continente_origen`, 'x')");
		array_push($concantenar, "COALESCE(`vuelos`.`tarifa`.`pais_origen`, 'x')");
		array_push($concantenar, "COALESCE(`vuelos`.`tarifa`.`ciudad_origen`, 'x')");
		array_push($concantenar, "COALESCE(`vuelos`.`tarifa`.`id_continente_destino`, 'x')");
		array_push($concantenar, "COALESCE(`vuelos`.`tarifa`.`pais_destino`, 'x')");
		array_push($concantenar, "COALESCE(`vuelos`.`tarifa`.`ciudad_destino`, 'x')");
		array_push($concantenar, "`vuelos`.`tarifa`.`id_portal`");
		array_push(self::$query, sprintf("UPDATE `vuelos`.`tarifa` SET `vuelos`.`tarifa`.`codigo` = CONCAT(%s);", implode(", '-', ", $concantenar)));
	}

	static function inicializaTablas() {
		array_push(self::$query, "-- INIT");
		array_push(self::$query, "SET FOREIGN_KEY_CHECKS=0;");
		array_push(self::$query, "TRUNCATE TABLE `vuelos`.`tarifa`;");
		array_push(self::$query, "TRUNCATE TABLE `vuelos`.`tarifa_mensaje`;");
		array_push(self::$query, "TRUNCATE TABLE `vuelos`.`tarifa_mensaje_tipo`;");
		array_push(self::$query, "TRUNCATE TABLE `vuelos`.`tarifa_regla_farebasis`;");
		array_push(self::$query, "SET FOREIGN_KEY_CHECKS=1;");
		self::saltoLinea(self::$query);
	}

	static function insertTipoTarifa() {
		array_push(self::$query, "-- INSERT TIPO TARIFA");
		foreach (self::$tipoMensajes as $tipoMensaje) {
			array_push(self::$query, sprintf("INSERT INTO `vuelos`.`tarifa_mensaje_tipo` (`codigo`, `descripcion`) VALUES ('%s', '');", $tipoMensaje));
			array_push(self::$query, sprintf("SET @id_tipo_mensaje_%s = (SELECT id_tarifa_mensaje_tipo FROM `vuelos`.`tarifa_mensaje_tipo` where `vuelos`.`tarifa_mensaje_tipo`.`codigo` = '%s');", $tipoMensaje, $tipoMensaje));
		}
		self::saltoLinea();
	}

	static function generaNombreArchivos() {
		$folder = "backup";
		$nombre = date("Y-m-d H-i-s");
		self::$fileCSV = $folder . DS . $nombre . ".csv";
		self::$fileSQL = $folder . DS . $nombre . ".sql";
	}

	static function generaSql() {
		if($archivo = fopen(self::$fileSQL, "a")) {
			$contenido = "";
			foreach(self::$query as $linea){
				fwrite($archivo, $linea.PHP_EOL);
			}
			fclose($archivo);
		}
	}

	static function generaCopiaCsvOriginal($path) {
		copy($path, self::$fileCSV);
	}

	static function main($path)
	{
		try {
			self::generaNombreArchivos();
			self::generaCopiaCsvOriginal($path);
			self::convierteCsvEnArray();
			self::identificaPrestadores();
			self::agruparReglas();
			self::generarConst();
			self::inicializaTablas();
			self::insertTipoTarifa();
			self::procesaReglasAgrupadas();
			self::fixQuery();
			self::imprimirReglas();
			self::generaSql();
		} catch (Exception $e) {
			print($e->getMessage());
		}
	}	
}

GeneraQueryPoliticas::main("politicas.csv");