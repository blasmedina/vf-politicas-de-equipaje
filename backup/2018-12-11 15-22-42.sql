-- PRESTADORES
SET @id_prestador_AR = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'AR');
SET @id_prestador_OY = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'OY');
SET @id_prestador_AV = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'AV');
SET @id_prestador_B6 = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'B6');
SET @id_prestador_LA = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'LA');
SET @id_prestador_AM = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'AM');
SET @id_prestador_AF = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'AF');
SET @id_prestador_KL = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'KL');
SET @id_prestador_UA = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'UA');
SET @id_prestador_AC = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'AC');
SET @id_prestador_AA = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'AA');
SET @id_prestador_CM = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'CM');
SET @id_prestador_EK = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'EK');
SET @id_prestador_AZ = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'AZ');
SET @id_prestador_BA = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'BA');
SET @id_prestador_O6 = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'O6');
SET @id_prestador_G3 = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'G3');
SET @id_prestador_DL = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'DL');
SET @id_prestador_QF = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'QF');
SET @id_prestador_IB = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'IB');
SET @id_prestador_H2 = (SELECT `id_prestador` FROM `viajes_falabella`.`prestador` WHERE `viajes_falabella`.`prestador`.`codigo` = 'H2');


-- PORTAL
SET @id_portal_cl = (SELECT `id_portal` FROM `viajes_falabella`.`portal` WHERE `viajes_falabella`.`portal`.`codigo_moneda` = 'CLP');
SET @id_portal_ar = (SELECT `id_portal` FROM `viajes_falabella`.`portal` WHERE `viajes_falabella`.`portal`.`codigo_moneda` = 'ARS');
SET @id_portal_co = (SELECT `id_portal` FROM `viajes_falabella`.`portal` WHERE `viajes_falabella`.`portal`.`codigo_moneda` = 'COP');
SET @id_portal_pe = (SELECT `id_portal` FROM `viajes_falabella`.`portal` WHERE `viajes_falabella`.`portal`.`codigo_moneda` = 'PEN');


-- CONTINENT
SET @id_continente_NA = (SELECT `idContinent` FROM `viajes_falabella`.`Continent` WHERE `viajes_falabella`.`Continent`.`description` = 'Norteamérica');
SET @id_continente_SA = (SELECT `idContinent` FROM `viajes_falabella`.`Continent` WHERE `viajes_falabella`.`Continent`.`description` = 'Sudamérica');
SET @id_continente_CA = (SELECT `idContinent` FROM `viajes_falabella`.`Continent` WHERE `viajes_falabella`.`Continent`.`description` = 'América Central');
SET @id_continente_EU = (SELECT `idContinent` FROM `viajes_falabella`.`Continent` WHERE `viajes_falabella`.`Continent`.`description` = 'Europa');
SET @id_continente_AF = (SELECT `idContinent` FROM `viajes_falabella`.`Continent` WHERE `viajes_falabella`.`Continent`.`description` = 'África');
SET @id_continente_AS = (SELECT `idContinent` FROM `viajes_falabella`.`Continent` WHERE `viajes_falabella`.`Continent`.`description` = 'Asia');
SET @id_continente_OC = (SELECT `idContinent` FROM `viajes_falabella`.`Continent` WHERE `viajes_falabella`.`Continent`.`description` = 'Oceanía');


-- INIT
SET FOREIGN_KEY_CHECKS=0;
TRUNCATE TABLE `vuelos`.`tarifa`;
TRUNCATE TABLE `vuelos`.`tarifa_mensaje`;
TRUNCATE TABLE `vuelos`.`tarifa_mensaje_tipo`;
TRUNCATE TABLE `vuelos`.`tarifa_regla_farebasis`;
SET FOREIGN_KEY_CHECKS=1;


-- INSERT TIPO TARIFA
INSERT INTO `vuelos`.`tarifa_mensaje_tipo` (`codigo`, `descripcion`) VALUES ('equipaje_bodega', '');
SET @id_tipo_mensaje_equipaje_bodega = (SELECT id_tarifa_mensaje_tipo FROM `vuelos`.`tarifa_mensaje_tipo` where `vuelos`.`tarifa_mensaje_tipo`.`codigo` = 'equipaje_bodega');
INSERT INTO `vuelos`.`tarifa_mensaje_tipo` (`codigo`, `descripcion`) VALUES ('equipaje_cabina', '');
SET @id_tipo_mensaje_equipaje_cabina = (SELECT id_tarifa_mensaje_tipo FROM `vuelos`.`tarifa_mensaje_tipo` where `vuelos`.`tarifa_mensaje_tipo`.`codigo` = 'equipaje_cabina');
INSERT INTO `vuelos`.`tarifa_mensaje_tipo` (`codigo`, `descripcion`) VALUES ('reembolsos', '');
SET @id_tipo_mensaje_reembolsos = (SELECT id_tarifa_mensaje_tipo FROM `vuelos`.`tarifa_mensaje_tipo` where `vuelos`.`tarifa_mensaje_tipo`.`codigo` = 'reembolsos');
INSERT INTO `vuelos`.`tarifa_mensaje_tipo` (`codigo`, `descripcion`) VALUES ('preseleccion_asiento', '');
SET @id_tipo_mensaje_preseleccion_asiento = (SELECT id_tarifa_mensaje_tipo FROM `vuelos`.`tarifa_mensaje_tipo` where `vuelos`.`tarifa_mensaje_tipo`.`codigo` = 'preseleccion_asiento');
INSERT INTO `vuelos`.`tarifa_mensaje_tipo` (`codigo`, `descripcion`) VALUES ('cambio_itinerario', '');
SET @id_tipo_mensaje_cambio_itinerario = (SELECT id_tarifa_mensaje_tipo FROM `vuelos`.`tarifa_mensaje_tipo` where `vuelos`.`tarifa_mensaje_tipo`.`codigo` = 'cambio_itinerario');
INSERT INTO `vuelos`.`tarifa_mensaje_tipo` (`codigo`, `descripcion`) VALUES ('upgrade', '');
SET @id_tipo_mensaje_upgrade = (SELECT id_tarifa_mensaje_tipo FROM `vuelos`.`tarifa_mensaje_tipo` where `vuelos`.`tarifa_mensaje_tipo`.`codigo` = 'upgrade');




-- TARIFA GENERICA PARA AEROLINEAS ARGENTINAS - CL - AEROLINEA ARGENTINAS
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'AEROLINEA_ARGENTINAS', 'Aerolinea Argentinas', 'Tarifa Generica para Aerolineas Argentinas', NULL, NULL, NULL, NULL, NULL, NULL, @id_prestador_AR);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 pieza de 15 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA AEROLINEAS ARGENTINAS - AR - AEROLINEA ARGENTINAS
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_ar, 'AEROLINEA_ARGENTINAS', 'Aerolinea Argentinas', 'Tarifa Generica para Aerolineas Argentinas', NULL, NULL, NULL, NULL, NULL, NULL, @id_prestador_AR);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 pieza de 15 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA AEROLINEAS ARGENTINAS - CO - AEROLINEA ARGENTINAS
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'AEROLINEA_ARGENTINAS', 'Aerolinea Argentinas', 'Tarifa Generica para Aerolineas Argentinas', NULL, NULL, NULL, NULL, NULL, NULL, @id_prestador_AR);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 pieza de 15 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA PARA AEROLINEAS ARGENTINAS VUELOS DOMESTICOS - AR - AEROLINEA ARGENTINAS
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_ar, 'AEROLINEA_ARGENTINAS', 'Aerolinea Argentinas', 'Tarifa para Aerolineas Argentinas vuelos domesticos', @id_continente_SA, 'AR', NULL, @id_continente_SA, 'AR', NULL, @id_prestador_AR);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('R_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', '', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA ANDES - CL - ANDES
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'ANDES', 'Andes', 'Tarifa Generica para Andes', NULL, NULL, NULL, NULL, NULL, NULL, @id_prestador_OY);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', 'Agrega una pieza de hasta 15 kg al hacer check-in en el aeropuerto. Sujeto a disponibilidad de espacio por parte de la aerolínea', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA ANDES - AR - ANDES
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_ar, 'ANDES', 'Andes', 'Tarifa Generica para Andes', NULL, NULL, NULL, NULL, NULL, NULL, @id_prestador_OY);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', 'Agrega una pieza de hasta 15 kg al hacer check-in en el aeropuerto. Sujeto a disponibilidad de espacio por parte de la aerolínea', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA ANDES - CO - ANDES
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ANDES', 'Andes', 'Tarifa Generica para Andes', NULL, NULL, NULL, NULL, NULL, NULL, @id_prestador_OY);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', 'Agrega una pieza de hasta 15 kg al hacer check-in en el aeropuerto. Sujeto a disponibilidad de espacio por parte de la aerolínea', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CANADá A CENTROAMERICA - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional de Canadá a Centroamerica', @id_continente_NA, 'CA', NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 150', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 200 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CANADá A SURAMERICA - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional de Canadá a Suramerica', @id_continente_NA, 'CA', NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 175', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 300 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A CANADá - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Canadá', @id_continente_CA, NULL, NULL, @id_continente_NA, 'CA', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 150', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 200 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A ESTADOS UNIDOS - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Estados Unidos', @id_continente_CA, NULL, NULL, @id_continente_NA, 'US', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 150', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 200 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A MEXICO - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Mexico', @id_continente_CA, NULL, NULL, @id_continente_NA, 'MX', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 50 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A SURAMERICA - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Suramerica', @id_continente_CA, NULL, NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 125 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE ESTADOS UNIDOS A CENTROAMERICA - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional de Estados Unidos a Centroamerica', @id_continente_NA, 'US', NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 150', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 200 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE ESTADOS UNIDOS A SURAMERICA - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional de Estados Unidos a Suramerica', @id_continente_NA, 'US', NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 175', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 300 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE MEXICO A CENTROAMERICA - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional de Mexico a Centroamerica', @id_continente_NA, 'MX', NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 50 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE MEXICO A SURAMERICA - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional de Mexico a Suramerica', @id_continente_NA, 'MX', NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 125 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A CANADá - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional de Suramerica a Canadá', @id_continente_SA, NULL, NULL, @id_continente_NA, 'CA', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 175', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 300 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A CENTROAMERICA - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional de Suramerica a Centroamerica', @id_continente_SA, NULL, NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 125 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A ESTADOS UNIDOS - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional de Suramerica a Estados Unidos', @id_continente_SA, NULL, NULL, @id_continente_NA, 'US', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 175', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 300 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A MEXICO - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional de Suramerica a Mexico', @id_continente_SA, NULL, NULL, @id_continente_NA, 'MX', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 125 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (ARUBA) - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional desde Caribe (Aruba)', @id_continente_CA, 'AW', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (CUBA) - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional desde Caribe (Cuba)', @id_continente_CA, 'CU', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (CURAZAO) - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional desde Caribe (Curazao)', @id_continente_CA, 'CB', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (EL SALVADOR) - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional desde Caribe (El Salvador)', @id_continente_CA, 'SV', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (REPúBLICA DOMINICANA) - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional desde Caribe (República Dominicana)', @id_continente_CA, 'DO', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE EUROPA - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional desde Europa', @id_continente_EU, NULL, NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 200/EUR 150/ GBP 123', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 170/EUR 120/ GBP 98 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE Y HACIA CARIBE (ARUBA) - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional desde y hacia Caribe (Aruba)', NULL, NULL, NULL, @id_continente_CA, 'AW', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL ENTRE CENTROAMERICA - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional entre Centroamerica', @id_continente_CA, NULL, NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 50 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL ENTRE SURAMERICA - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional entre Suramerica', @id_continente_SA, NULL, NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (CUBA) - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional hacia Caribe (Cuba)', NULL, NULL, NULL, @id_continente_CA, 'CU', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (CURAZAO) - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional hacia Caribe (Curazao)', NULL, NULL, NULL, @id_continente_CA, 'CB', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (EL SALVADOR) - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional hacia Caribe (El Salvador)', NULL, NULL, NULL, @id_continente_CA, 'SV', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (REPúBLICA DOMINICANA) - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional hacia Caribe (República Dominicana)', NULL, NULL, NULL, @id_continente_CA, 'DO', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA EUROPA - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Internacional hacia Europa', NULL, NULL, NULL, @id_continente_EU, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 200/EUR 150/ GBP 123', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 170/EUR 120/ GBP 98 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS NACIONALES - CO - ECONO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'ECONO', 'Econo', 'Tarifa para Avianca vuelos Nacionales', @id_continente_SA, 'CO', NULL, @id_continente_SA, 'CO', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('Z_', @id_tarifa), ('P_', @id_tarifa), ('O_', @id_tarifa), ('L_', @id_tarifa), ('E_', @id_tarifa), ('Q_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable con cobro adicional de COP 100.000', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de COP 85.000 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CANADá A CENTROAMERICA - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional de Canadá a Centroamerica', @id_continente_NA, 'CA', NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CANADá A SURAMERICA - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional de Canadá a Suramerica', @id_continente_NA, 'CA', NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A CANADá - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Canadá', @id_continente_CA, NULL, NULL, @id_continente_NA, 'CA', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A ESTADOS UNIDOS - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Estados Unidos', @id_continente_CA, NULL, NULL, @id_continente_NA, 'US', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A MEXICO - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Mexico', @id_continente_CA, NULL, NULL, @id_continente_NA, 'MX', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A SURAMERICA - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Suramerica', @id_continente_CA, NULL, NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE ESTADOS UNIDOS A CENTROAMERICA - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional de Estados Unidos a Centroamerica', @id_continente_NA, 'US', NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE ESTADOS UNIDOS A SURAMERICA - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional de Estados Unidos a Suramerica', @id_continente_NA, 'US', NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE MEXICO A CENTROAMERICA - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional de Mexico a Centroamerica', @id_continente_NA, 'MX', NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE MEXICO A SURAMERICA - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional de Mexico a Suramerica', @id_continente_NA, 'MX', NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 125 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A CANADá - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional de Suramerica a Canadá', @id_continente_SA, NULL, NULL, @id_continente_NA, 'CA', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A CENTROAMERICA - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional de Suramerica a Centroamerica', @id_continente_SA, NULL, NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A ESTADOS UNIDOS - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional de Suramerica a Estados Unidos', @id_continente_SA, NULL, NULL, @id_continente_NA, 'US', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A MEXICO - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional de Suramerica a Mexico', @id_continente_SA, NULL, NULL, @id_continente_NA, 'MX', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 125 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (ARUBA) - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional desde Caribe (Aruba)', @id_continente_CA, 'AW', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (CUBA) - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional desde Caribe (Cuba)', @id_continente_CA, 'CU', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (CURAZAO) - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional desde Caribe (Curazao)', @id_continente_CA, 'CB', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (EL SALVADOR) - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional desde Caribe (El Salvador)', @id_continente_CA, 'SV', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (REPúBLICA DOMINICANA) - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional desde Caribe (República Dominicana)', @id_continente_CA, 'DO', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE EUROPA - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional desde Europa', @id_continente_EU, NULL, NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE Y HACIA CARIBE (ARUBA) - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional desde y hacia Caribe (Aruba)', NULL, NULL, NULL, @id_continente_CA, 'AW', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL ENTRE CENTROAMERICA - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional entre Centroamerica', @id_continente_CA, NULL, NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL ENTRE SURAMERICA - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional entre Suramerica', @id_continente_SA, NULL, NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (CUBA) - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional hacia Caribe (Cuba)', NULL, NULL, NULL, @id_continente_CA, 'CU', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (CURAZAO) - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional hacia Caribe (Curazao)', NULL, NULL, NULL, @id_continente_CA, 'CB', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (EL SALVADOR) - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional hacia Caribe (El Salvador)', NULL, NULL, NULL, @id_continente_CA, 'SV', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (REPúBLICA DOMINICANA) - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional hacia Caribe (República Dominicana)', NULL, NULL, NULL, @id_continente_CA, 'DO', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA EUROPA - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Internacional hacia Europa', NULL, NULL, NULL, @id_continente_EU, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS NACIONALES - CO - EJECUTIVA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA', 'Ejecutiva', 'Tarifa para Avianca vuelos Nacionales', @id_continente_SA, 'CO', NULL, @id_continente_SA, 'CO', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('C_', @id_tarifa), ('J_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', NULL, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', NULL, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido sin cobro adicional', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CANADá A CENTROAMERICA - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional de Canadá a Centroamerica', @id_continente_NA, 'CA', NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 150', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 200 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CANADá A SURAMERICA - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional de Canadá a Suramerica', @id_continente_NA, 'CA', NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 175', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A CANADá - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Canadá', @id_continente_CA, NULL, NULL, @id_continente_NA, 'CA', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 150', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 200 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A ESTADOS UNIDOS - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Estados Unidos', @id_continente_CA, NULL, NULL, @id_continente_NA, 'US', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 150', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 200 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A MEXICO - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Mexico', @id_continente_CA, NULL, NULL, @id_continente_NA, 'MX', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 50 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A SURAMERICA - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Suramerica', @id_continente_CA, NULL, NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 125 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE ESTADOS UNIDOS A CENTROAMERICA - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional de Estados Unidos a Centroamerica', @id_continente_NA, 'US', NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 150', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 200 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE ESTADOS UNIDOS A SURAMERICA - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional de Estados Unidos a Suramerica', @id_continente_NA, 'US', NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 175', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 300 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE MEXICO A CENTROAMERICA - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional de Mexico a Centroamerica', @id_continente_NA, 'MX', NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 50 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE MEXICO A SURAMERICA - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional de Mexico a Suramerica', @id_continente_NA, 'MX', NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A CANADá - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional de Suramerica a Canadá', @id_continente_SA, NULL, NULL, @id_continente_NA, 'CA', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 175', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A CENTROAMERICA - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional de Suramerica a Centroamerica', @id_continente_SA, NULL, NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 125 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A ESTADOS UNIDOS - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional de Suramerica a Estados Unidos', @id_continente_SA, NULL, NULL, @id_continente_NA, 'US', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 175', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 300 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A MEXICO - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional de Suramerica a Mexico', @id_continente_SA, NULL, NULL, @id_continente_NA, 'MX', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (ARUBA) - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional desde Caribe (Aruba)', @id_continente_CA, 'AW', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (CUBA) - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional desde Caribe (Cuba)', @id_continente_CA, 'CU', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (CURAZAO) - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional desde Caribe (Curazao)', @id_continente_CA, 'CB', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (EL SALVADOR) - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional desde Caribe (El Salvador)', @id_continente_CA, 'SV', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (REPúBLICA DOMINICANA) - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional desde Caribe (República Dominicana)', @id_continente_CA, 'DO', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE EUROPA - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional desde Europa', @id_continente_EU, NULL, NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 200/EUR 150/ GBP 123', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 170/EUR 120/ GBP 98 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE Y HACIA CARIBE (ARUBA) - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional desde y hacia Caribe (Aruba)', NULL, NULL, NULL, @id_continente_CA, 'AW', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL ENTRE CENTROAMERICA - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional entre Centroamerica', @id_continente_CA, NULL, NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 50 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL ENTRE SURAMERICA - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional entre Suramerica', @id_continente_SA, NULL, NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (CUBA) - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional hacia Caribe (Cuba)', NULL, NULL, NULL, @id_continente_CA, 'CU', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (CURAZAO) - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional hacia Caribe (Curazao)', NULL, NULL, NULL, @id_continente_CA, 'CB', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (EL SALVADOR) - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional hacia Caribe (El Salvador)', NULL, NULL, NULL, @id_continente_CA, 'SV', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (REPúBLICA DOMINICANA) - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional hacia Caribe (República Dominicana)', NULL, NULL, NULL, @id_continente_CA, 'DO', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 100', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 75 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA EUROPA - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Internacional hacia Europa', NULL, NULL, NULL, @id_continente_EU, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de USD 200/EUR 150/ GBP 123', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 170/EUR 120/ GBP 98 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS NACIONALES - CO - EJECUTIVA PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'EJECUTIVA_PROMO', 'Ejecutiva Promo', 'Tarifa para Avianca vuelos Nacionales', @id_continente_SA, 'CO', NULL, @id_continente_SA, 'CO', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('A_', @id_tarifa), ('D_', @id_tarifa), ('K_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 32 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Permitido con cobro adicional de COP 100.000', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de COP 85.000 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'No aplica', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CANADá A CENTROAMERICA - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional de Canadá a Centroamerica', @id_continente_NA, 'CA', NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CANADá A SURAMERICA - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional de Canadá a Suramerica', @id_continente_NA, 'CA', NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A CANADá - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Canadá', @id_continente_CA, NULL, NULL, @id_continente_NA, 'CA', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A ESTADOS UNIDOS - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Estados Unidos', @id_continente_CA, NULL, NULL, @id_continente_NA, 'US', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A MEXICO - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Mexico', @id_continente_CA, NULL, NULL, @id_continente_NA, 'MX', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A SURAMERICA - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Suramerica', @id_continente_CA, NULL, NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE ESTADOS UNIDOS A CENTROAMERICA - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional de Estados Unidos a Centroamerica', @id_continente_NA, 'US', NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE ESTADOS UNIDOS A SURAMERICA - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional de Estados Unidos a Suramerica', @id_continente_NA, 'US', NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE MEXICO A CENTROAMERICA - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional de Mexico a Centroamerica', @id_continente_NA, 'MX', NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE MEXICO A SURAMERICA - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional de Mexico a Suramerica', @id_continente_NA, 'MX', NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A CANADá - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional de Suramerica a Canadá', @id_continente_SA, NULL, NULL, @id_continente_NA, 'CA', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A CENTROAMERICA - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional de Suramerica a Centroamerica', @id_continente_SA, NULL, NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A ESTADOS UNIDOS - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional de Suramerica a Estados Unidos', @id_continente_SA, NULL, NULL, @id_continente_NA, 'US', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A MEXICO - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional de Suramerica a Mexico', @id_continente_SA, NULL, NULL, @id_continente_NA, 'MX', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (ARUBA) - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional desde Caribe (Aruba)', @id_continente_CA, 'AW', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (CUBA) - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional desde Caribe (Cuba)', @id_continente_CA, 'CU', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (CURAZAO) - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional desde Caribe (Curazao)', @id_continente_CA, 'CB', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (EL SALVADOR) - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional desde Caribe (El Salvador)', @id_continente_CA, 'SV', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (REPúBLICA DOMINICANA) - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional desde Caribe (República Dominicana)', @id_continente_CA, 'DO', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE EUROPA - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional desde Europa', @id_continente_EU, NULL, NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE Y HACIA CARIBE (ARUBA) - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional desde y hacia Caribe (Aruba)', NULL, NULL, NULL, @id_continente_CA, 'AW', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL ENTRE CENTROAMERICA - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional entre Centroamerica', @id_continente_CA, NULL, NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL ENTRE SURAMERICA - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional entre Suramerica', @id_continente_SA, NULL, NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (CUBA) - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional hacia Caribe (Cuba)', NULL, NULL, NULL, @id_continente_CA, 'CU', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (CURAZAO) - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional hacia Caribe (Curazao)', NULL, NULL, NULL, @id_continente_CA, 'CB', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (EL SALVADOR) - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional hacia Caribe (El Salvador)', NULL, NULL, NULL, @id_continente_CA, 'SV', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (REPúBLICA DOMINICANA) - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional hacia Caribe (República Dominicana)', NULL, NULL, NULL, @id_continente_CA, 'DO', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA EUROPA - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Internacional hacia Europa', NULL, NULL, NULL, @id_continente_EU, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS NACIONALES - CO - FLEXI
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'FLEXI', 'Flexi', 'Tarifa para Avianca vuelos Nacionales', @id_continente_SA, 'CO', NULL, @id_continente_SA, 'CO', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('H_', @id_tarifa), ('M_', @id_tarifa), ('B_', @id_tarifa), ('V_', @id_tarifa), ('Y_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'Reembolsable sin cobro adicional', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional equivalente a la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CANADá A CENTROAMERICA - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional de Canadá a Centroamerica', @id_continente_NA, 'CA', NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 200 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CANADá A SURAMERICA - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional de Canadá a Suramerica', @id_continente_NA, 'CA', NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 300 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A CANADá - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Canadá', @id_continente_CA, NULL, NULL, @id_continente_NA, 'CA', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 200 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A ESTADOS UNIDOS - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Estados Unidos', @id_continente_CA, NULL, NULL, @id_continente_NA, 'US', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 200 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A MEXICO - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Mexico', @id_continente_CA, NULL, NULL, @id_continente_NA, 'MX', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 115 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE CENTROAMERICA A SURAMERICA - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional de Centroamerica a Suramerica', @id_continente_CA, NULL, NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 150 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE ESTADOS UNIDOS A CENTROAMERICA - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional de Estados Unidos a Centroamerica', @id_continente_NA, 'US', NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 200 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE ESTADOS UNIDOS A SURAMERICA - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional de Estados Unidos a Suramerica', @id_continente_NA, 'US', NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 300 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE MEXICO A CENTROAMERICA - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional de Mexico a Centroamerica', @id_continente_NA, 'MX', NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 115 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE MEXICO A SURAMERICA - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional de Mexico a Suramerica', @id_continente_NA, 'MX', NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 150 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A CANADá - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional de Suramerica a Canadá', @id_continente_SA, NULL, NULL, @id_continente_NA, 'CA', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 300 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A CENTROAMERICA - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional de Suramerica a Centroamerica', @id_continente_SA, NULL, NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 150 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A ESTADOS UNIDOS - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional de Suramerica a Estados Unidos', @id_continente_SA, NULL, NULL, @id_continente_NA, 'US', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 300 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DE SURAMERICA A MEXICO - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional de Suramerica a Mexico', @id_continente_SA, NULL, NULL, @id_continente_NA, 'MX', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 150 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (ARUBA) - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional desde Caribe (Aruba)', @id_continente_CA, 'AW', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 115 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (CUBA) - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional desde Caribe (Cuba)', @id_continente_CA, 'CU', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 115 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (CURAZAO) - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional desde Caribe (Curazao)', @id_continente_CA, 'CB', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 115 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (EL SALVADOR) - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional desde Caribe (El Salvador)', @id_continente_CA, 'SV', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 115 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE CARIBE (REPúBLICA DOMINICANA) - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional desde Caribe (República Dominicana)', @id_continente_CA, 'DO', NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 115 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE EUROPA - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional desde Europa', @id_continente_EU, NULL, NULL, NULL, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 190/EUR 150/ GBP 98 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL DESDE Y HACIA CARIBE (ARUBA) - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional desde y hacia Caribe (Aruba)', NULL, NULL, NULL, @id_continente_CA, 'AW', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 115 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa), ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL ENTRE CENTROAMERICA - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional entre Centroamerica', @id_continente_CA, NULL, NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 115 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL ENTRE SURAMERICA - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional entre Suramerica', @id_continente_SA, NULL, NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 125 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (CUBA) - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional hacia Caribe (Cuba)', NULL, NULL, NULL, @id_continente_CA, 'CU', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 115 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (CURAZAO) - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional hacia Caribe (Curazao)', NULL, NULL, NULL, @id_continente_CA, 'CB', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 115 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (EL SALVADOR) - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional hacia Caribe (El Salvador)', NULL, NULL, NULL, @id_continente_CA, 'SV', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 115 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA CARIBE (REPúBLICA DOMINICANA) - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional hacia Caribe (República Dominicana)', NULL, NULL, NULL, @id_continente_CA, 'DO', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 115 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS INTERNACIONAL HACIA EUROPA - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Internacional hacia Europa', NULL, NULL, NULL, @id_continente_EU, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kilogramos cada una', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de USD 190/EUR 150/ GBP 98 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA AVIANCA VUELOS NACIONALES - CO - SUPER PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'SUPER_PROMO', 'Super Promo', 'Tarifa para Avianca vuelos Nacionales', @id_continente_SA, 'CO', NULL, @id_continente_SA, 'CO', NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('U_', @id_tarifa), ('T_', @id_tarifa), ('S_', @id_tarifa), ('W_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 piezas de 23 kilogramos', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', 1, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'Permitido con cobro adicional', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'Permitido con cobro adicional de COP 120.000 más la diferencia tarifaria', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa), ('Ascenso de clase', 'Aplica para socios LifeMiles Élite', NULL, @id_tipo_mensaje_upgrade, @id_tarifa);


-- TARIFA PARA JETBLUE - CL - JETBLUE
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'JETBLUE', 'JetBlue', 'Tarifa para JetBlue', NULL, NULL, NULL, NULL, NULL, NULL, @id_prestador_B6);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', 'La aerolínea JetBlue no permite transportar equipaje en bodega', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA PARA JETBLUE - AR - JETBLUE
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_ar, 'JETBLUE', 'JetBlue', 'Tarifa para JetBlue', NULL, NULL, NULL, NULL, NULL, NULL, @id_prestador_B6);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', 'La aerolínea JetBlue no permite transportar equipaje en bodega', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA PARA JETBLUE - CO - JETBLUE
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'JETBLUE', 'JetBlue', 'Tarifa para JetBlue', NULL, NULL, NULL, NULL, NULL, NULL, @id_prestador_B6);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', 'La aerolínea JetBlue no permite transportar equipaje en bodega', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA PARA JETBLUE - PE - JETBLUE
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_pe, 'JETBLUE', 'JetBlue', 'Tarifa para JetBlue', NULL, NULL, NULL, NULL, NULL, NULL, @id_prestador_B6);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', 'La aerolínea JetBlue no permite transportar equipaje en bodega', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA LIGHT PARA LATAM VUELOS NACIONALES EN CHILE - CL - LIGHT
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'LIGHT', 'Light', 'Tarifa Light para Latam vuelos Nacionales en Chile', @id_continente_SA, 'CL', NULL, @id_continente_SA, 'CL', NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('___SL_', @id_tarifa), ('___LE_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', 'Añade equipaje en bodega pagando en el sitio web de cada aerolínea o en el aeropuerto', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'No permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'No permitido', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa);


-- TARIFA LIGHT PARA LATAM VUELOS NACIONALES EN CHILE - CO - LIGHT
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'LIGHT', 'Light', 'Tarifa Light para Latam vuelos Nacionales en Chile', @id_continente_SA, 'CL', NULL, @id_continente_SA, 'CL', NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('___SL_', @id_tarifa), ('___LE_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', 'Añade equipaje en bodega pagando en el sitio web de cada aerolínea o en el aeropuerto', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'No permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'No permitido', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa);


-- TARIFA LIGHT PARA LATAM VUELOS NACIONALES EN CHILE - PE - LIGHT
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_pe, 'LIGHT', 'Light', 'Tarifa Light para Latam vuelos Nacionales en Chile', @id_continente_SA, 'CL', NULL, @id_continente_SA, 'CL', NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('___SL_', @id_tarifa), ('___LE_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', 'Añade equipaje en bodega pagando en el sitio web de cada aerolínea o en el aeropuerto', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'No permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'No permitido', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa);


-- TARIFA LIGHT PARA LATAM VUELOS NACIONALES EN COLOMBIA - CL - LIGHT
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'LIGHT', 'Light', 'Tarifa Light para Latam vuelos Nacionales en Colombia', @id_continente_SA, 'CO', NULL, @id_continente_SA, 'CO', NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('___SL_', @id_tarifa), ('___LE_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', 'Añade equipaje en bodega pagando en el sitio web de cada aerolínea o en el aeropuerto', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'No permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'No permitido', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa);


-- TARIFA LIGHT PARA LATAM VUELOS NACIONALES EN COLOMBIA - CO - LIGHT
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'LIGHT', 'Light', 'Tarifa Light para Latam vuelos Nacionales en Colombia', @id_continente_SA, 'CO', NULL, @id_continente_SA, 'CO', NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('___SL_', @id_tarifa), ('___LE_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', 'Añade equipaje en bodega pagando en el sitio web de cada aerolínea o en el aeropuerto', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'No permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'No permitido', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa);


-- TARIFA LIGHT PARA LATAM VUELOS NACIONALES EN COLOMBIA - PE - LIGHT
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_pe, 'LIGHT', 'Light', 'Tarifa Light para Latam vuelos Nacionales en Colombia', @id_continente_SA, 'CO', NULL, @id_continente_SA, 'CO', NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('___SL_', @id_tarifa), ('___LE_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', 'Añade equipaje en bodega pagando en el sitio web de cada aerolínea o en el aeropuerto', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'No permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'No permitido', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa);


-- TARIFA LIGHT PARA LATAM VUELOS NACIONALES EN PERU - CL - LIGHT
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'LIGHT', 'Light', 'Tarifa Light para Latam vuelos Nacionales en Peru', @id_continente_SA, 'PE', NULL, @id_continente_SA, 'PE', NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('___SL_', @id_tarifa), ('___LE_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', 'Añade equipaje en bodega pagando en el sitio web de cada aerolínea o en el aeropuerto', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'No permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'No permitido', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa);


-- TARIFA LIGHT PARA LATAM VUELOS NACIONALES EN PERU - CO - LIGHT
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'LIGHT', 'Light', 'Tarifa Light para Latam vuelos Nacionales en Peru', @id_continente_SA, 'PE', NULL, @id_continente_SA, 'PE', NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('___SL_', @id_tarifa), ('___LE_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', 'Añade equipaje en bodega pagando en el sitio web de cada aerolínea o en el aeropuerto', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'No permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'No permitido', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa);


-- TARIFA LIGHT PARA LATAM VUELOS NACIONALES EN PERU - PE - LIGHT
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_pe, 'LIGHT', 'Light', 'Tarifa Light para Latam vuelos Nacionales en Peru', @id_continente_SA, 'PE', NULL, @id_continente_SA, 'PE', NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('___SL_', @id_tarifa), ('___LE_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', 'Añade equipaje en bodega pagando en el sitio web de cada aerolínea o en el aeropuerto', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'No permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'No permitido', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa);


-- TARIFA GENERICA PARA NACIONAL SKY - CL - LIGHT
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'LIGHT', 'LIGHT', 'Tarifa generica para nacional SKY', @id_continente_SA, 'CL', NULL, @id_continente_NA, NULL, NULL, @id_prestador_H2);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de mano', '1 pieza 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('No incluye equipaje en bodega', 'Añade equipaje en bodega pagando en el sitio web de cada aerolínea o en el aeropuerto', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa);


-- TARIFA GENERICA PARA NACIONAL SKY - CL - LIGHT
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'LIGHT', 'LIGHT', 'Tarifa generica para nacional SKY', @id_continente_SA, 'CL', NULL, @id_continente_SA, NULL, NULL, @id_prestador_H2);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de mano', '1 pieza 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('No incluye equipaje en bodega', 'Añade equipaje en bodega pagando en el sitio web de cada aerolínea o en el aeropuerto', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa);


-- TARIFA PROMO PARA LATAM VUELOS NACIONALES EN CHILE - CL - PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'PROMO', 'Promo', 'Tarifa Promo para Latam vuelos Nacionales en Chile', @id_continente_SA, 'CL', NULL, @id_continente_SA, 'CL', NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('___SN_', @id_tarifa), ('___SP_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', '', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'No permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'No permitido', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa);


-- TARIFA PROMO PARA LATAM VUELOS NACIONALES EN CHILE - CO - PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'PROMO', 'Promo', 'Tarifa Promo para Latam vuelos Nacionales en Chile', @id_continente_SA, 'CL', NULL, @id_continente_SA, 'CL', NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('___SN_', @id_tarifa), ('___SP_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', '', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'No permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'No permitido', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa);


-- TARIFA PROMO PARA LATAM VUELOS NACIONALES EN CHILE - PE - PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_pe, 'PROMO', 'Promo', 'Tarifa Promo para Latam vuelos Nacionales en Chile', @id_continente_SA, 'CL', NULL, @id_continente_SA, 'CL', NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('___SN_', @id_tarifa), ('___SP_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', '', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'No permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'No permitido', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa);


-- TARIFA PROMO PARA LATAM VUELOS NACIONALES EN COLOMBIA - CL - PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'PROMO', 'Promo', 'Tarifa Promo para Latam vuelos Nacionales en Colombia', @id_continente_SA, 'CO', NULL, @id_continente_SA, 'CO', NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('___SN_', @id_tarifa), ('___SP_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', '', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'No permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'No permitido', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa);


-- TARIFA PROMO PARA LATAM VUELOS NACIONALES EN COLOMBIA - CO - PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'PROMO', 'Promo', 'Tarifa Promo para Latam vuelos Nacionales en Colombia', @id_continente_SA, 'CO', NULL, @id_continente_SA, 'CO', NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('___SN_', @id_tarifa), ('___SP_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', '', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'No permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'No permitido', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa);


-- TARIFA PROMO PARA LATAM VUELOS NACIONALES EN COLOMBIA - PE - PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_pe, 'PROMO', 'Promo', 'Tarifa Promo para Latam vuelos Nacionales en Colombia', @id_continente_SA, 'CO', NULL, @id_continente_SA, 'CO', NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('___SN_', @id_tarifa), ('___SP_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', '', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'No permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'No permitido', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa);


-- TARIFA PROMO PARA LATAM VUELOS NACIONALES EN PERU - CL - PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'PROMO', 'Promo', 'Tarifa Promo para Latam vuelos Nacionales en Peru', @id_continente_SA, 'PE', NULL, @id_continente_SA, 'PE', NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('___SN_', @id_tarifa), ('___SP_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', '', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'No permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'No permitido', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa);


-- TARIFA PROMO PARA LATAM VUELOS NACIONALES EN PERU - CO - PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_co, 'PROMO', 'Promo', 'Tarifa Promo para Latam vuelos Nacionales en Peru', @id_continente_SA, 'PE', NULL, @id_continente_SA, 'PE', NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('___SN_', @id_tarifa), ('___SP_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', '', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'No permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'No permitido', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa);


-- TARIFA PROMO PARA LATAM VUELOS NACIONALES EN PERU - PE - PROMO
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_pe, 'PROMO', 'Promo', 'Tarifa Promo para Latam vuelos Nacionales en Peru', @id_continente_SA, 'PE', NULL, @id_continente_SA, 'PE', NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- REGLAS FAREBASIS
INSERT INTO `vuelos`.`tarifa_regla_farebasis` (`regla`, `id_tarifa`) VALUES ('___SN_', @id_tarifa), ('___SP_', @id_tarifa);
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', '', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kilogramos', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('Cancelación', 'No permitido', NULL, @id_tipo_mensaje_reembolsos, @id_tarifa), ('Selección de asiento', 'No permitido', NULL, @id_tipo_mensaje_preseleccion_asiento, @id_tarifa), ('Cambios', 'No permitido', NULL, @id_tipo_mensaje_cambio_itinerario, @id_tarifa);


-- TARIFA GENERICA PARA AEROMEXICO ASIA - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Aeromexico Asia', @id_continente_SA, NULL, NULL, @id_continente_AS, NULL, NULL, @id_prestador_AM);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kg c/u', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '2 piezas de 10 kg en total', 2, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA AEROMEXICO CENTROAMERICA - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Aeromexico Centroamerica', @id_continente_SA, NULL, NULL, @id_continente_CA, NULL, NULL, @id_prestador_AM);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 pieza de 23 kg', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '2 piezas de 10 kg en total', 2, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA AEROMEXICO EUROPA - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Aeromexico Europa', @id_continente_SA, NULL, NULL, @id_continente_EU, NULL, NULL, @id_prestador_AM);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 pieza de 23 kg', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '2 piezas de 10 kg en total', 2, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA AEROMEXICO NORTEAMERICA - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Aeromexico Norteamerica', @id_continente_SA, NULL, NULL, @id_continente_NA, NULL, NULL, @id_prestador_AM);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 pieza de 23 kg', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '2 piezas de 10 kg en total', 2, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA AFRICA LATAM - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Africa Latam', @id_continente_SA, NULL, NULL, @id_continente_AF, NULL, NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kg c/u', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA AIRFRANCE EUROPA - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Airfrance Europa', @id_continente_SA, NULL, NULL, @id_continente_EU, NULL, NULL, @id_prestador_AF);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', 'Añade equipaje en bodega pagando en el sitio web de cada aerolínea o en el aeropuerto', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 12 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA AIRFRANCE EUROPA - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Airfrance Europa', @id_continente_SA, NULL, NULL, @id_continente_EU, NULL, NULL, @id_prestador_KL);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', 'Añade equipaje en bodega pagando en el sitio web de cada aerolínea o en el aeropuerto', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 12 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA ASIA UNITED - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Asia United', @id_continente_SA, 'CL', NULL, @id_continente_AS, NULL, NULL, @id_prestador_UA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kg c/u', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA BUENOS AIRES AIR CANADá - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Buenos Aires Air Canadá', @id_continente_SA, 'CL', NULL, @id_continente_SA, 'AR', 'BUE', @id_prestador_AC);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kg c/u', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA CARIBE AMERICAN - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Caribe American', @id_continente_SA, NULL, NULL, @id_continente_CA, NULL, NULL, @id_prestador_AA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kg c/u', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA CARIBE AVIANCA - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Caribe Avianca', @id_continente_SA, NULL, NULL, @id_continente_CA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 pieza de 23 kg', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA CARIBE COPA - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Caribe Copa', @id_continente_SA, NULL, NULL, @id_continente_CA, NULL, NULL, @id_prestador_CM);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 pieza de 23 kg', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA CARIBE LATAM - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Caribe Latam', @id_continente_SA, NULL, NULL, @id_continente_CA, NULL, NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 pieza de 23 kg', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA EMIRATES - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Emirates', @id_continente_SA, 'CL', NULL, @id_continente_AS, NULL, NULL, @id_prestador_EK);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kg c/u', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA EUROPA ALITALIA - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Europa Alitalia', @id_continente_SA, 'CL', NULL, @id_continente_EU, NULL, NULL, @id_prestador_AZ);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 pieza de 23 kg', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA EUROPA AMERICAN - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Europa American', @id_continente_SA, NULL, NULL, @id_continente_EU, NULL, NULL, @id_prestador_AA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kg c/u', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA EUROPA AVIANCA - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Europa Avianca', @id_continente_SA, NULL, NULL, @id_continente_EU, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 pieza de 23 kg', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA EUROPA BRITISH - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Europa British', @id_continente_SA, 'CL', NULL, @id_continente_EU, NULL, NULL, @id_prestador_BA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 pieza de 23 kg', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('No incluye equipaje en bodega', 'Añade equipaje en bodega pagando en el sitio web de cada aerolínea o en el aeropuerto', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA EUROPA BRITISH - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Europa British', @id_continente_SA, 'CL', NULL, @id_continente_SA, 'BR', NULL, @id_prestador_O6);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 pieza de 23 kg', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA EUROPA UNITED - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Europa United', @id_continente_SA, 'CL', NULL, @id_continente_EU, NULL, NULL, @id_prestador_UA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kg c/u', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA GOL - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Gol', @id_continente_SA, 'CL', NULL, @id_continente_SA, 'BR', NULL, @id_prestador_G3);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 pieza de 23 kg', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA NORTEAMERICA AIR CANADá - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Norteamerica Air Canadá', @id_continente_SA, 'CL', NULL, @id_continente_NA, NULL, NULL, @id_prestador_AC);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kg c/u', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA NORTEAMéRICA AMERICAN - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Norteamérica American', @id_continente_SA, NULL, NULL, @id_continente_NA, NULL, NULL, @id_prestador_AA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kg c/u', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA NORTEAMéRICA AVIANCA - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Norteamérica Avianca', @id_continente_SA, NULL, NULL, @id_continente_NA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 pieza de 23 kg', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA NORTEAMéRICA COPA - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Norteamérica Copa', @id_continente_SA, NULL, NULL, @id_continente_NA, NULL, NULL, @id_prestador_CM);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 pieza de 23 kg', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA NORTEAMéRICA DELTA - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Norteamérica Delta', @id_continente_SA, 'CL', NULL, @id_continente_NA, 'US', NULL, @id_prestador_DL);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kg c/u', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA NORTEAMéRICA LATAM - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Norteamérica Latam', @id_continente_SA, NULL, NULL, @id_continente_NA, NULL, NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kg c/u', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA NORTEAMéRICA UNITED - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Norteamérica United', @id_continente_SA, 'CL', NULL, @id_continente_NA, NULL, NULL, @id_prestador_UA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kg c/u', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA OCEANíA LATAM - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Oceanía Latam', @id_continente_SA, NULL, NULL, @id_continente_OC, NULL, NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kg c/u', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA QANTAS OCEANIA - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Qantas Oceania', @id_continente_SA, 'CL', NULL, @id_continente_OC, NULL, NULL, @id_prestador_QF);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '2 piezas de 23 kg c/u', 2, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 7 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA SUDAMéRICA AVIANCA - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Sudamérica Avianca', @id_continente_SA, NULL, NULL, @id_continente_SA, NULL, NULL, @id_prestador_AV);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 pieza de 23 kg', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA SUDAMéRICA COPA - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Sudamérica Copa', @id_continente_SA, NULL, NULL, @id_continente_SA, NULL, NULL, @id_prestador_CM);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de bodega', '1 pieza de 23 kg', 1, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 10 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA SUDAMéRICA LATAM - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Sudamérica Latam', @id_continente_SA, NULL, NULL, @id_continente_SA, NULL, NULL, @id_prestador_LA);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('No incluye equipaje en bodega', 'Añade equipaje en bodega pagando en el sitio web de cada aerolínea o en el aeropuerto', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa), ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa);


-- TARIFA GENERICA PARA IBERIA - CL - GENERICA
INSERT INTO `vuelos`.`tarifa` (`id_portal`, `codigo`, `nombre`, `descripcion`, `id_continente_origen`, `pais_origen`, `ciudad_origen`, `id_continente_destino`, `pais_destino`, `ciudad_destino`, `id_prestador`) VALUES (@id_portal_cl, 'GENERICA', 'Generica', 'Tarifa Generica para Iberia', @id_continente_SA, 'CL', NULL, @id_continente_EU, NULL, NULL, @id_prestador_IB);
SET @id_tarifa = (SELECT LAST_INSERT_ID());
-- MENSAJES
INSERT INTO `vuelos`.`tarifa_mensaje` (`titulo`, `mensaje`, `valor`, `id_tarifa_mensaje_tipo`, `id_tarifa`) VALUES ('Incluye equipaje de mano', '1 pieza de 8 kg', 1, @id_tipo_mensaje_equipaje_cabina, @id_tarifa), ('No incluye equipaje en bodega', 'Añade equipaje en bodega pagando en el sitio web de cada aerolínea o en el aeropuerto', 0, @id_tipo_mensaje_equipaje_bodega, @id_tarifa);


-- FIX CODIGO
UPDATE `vuelos`.`tarifa` SET `vuelos`.`tarifa`.`codigo` = CONCAT(`vuelos`.`tarifa`.`codigo`, '-', COALESCE(`vuelos`.`tarifa`.`id_continente_origen`, 'x'), '-', COALESCE(`vuelos`.`tarifa`.`pais_origen`, 'x'), '-', COALESCE(`vuelos`.`tarifa`.`ciudad_origen`, 'x'), '-', COALESCE(`vuelos`.`tarifa`.`id_continente_destino`, 'x'), '-', COALESCE(`vuelos`.`tarifa`.`pais_destino`, 'x'), '-', COALESCE(`vuelos`.`tarifa`.`ciudad_destino`, 'x'), '-', `vuelos`.`tarifa`.`id_portal`);
